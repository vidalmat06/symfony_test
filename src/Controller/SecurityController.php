<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_register")
     */
    public function register(Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $encoder) : Response
    {
        $user = new User();

        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $user->setRoles(["ROLE_USER"]);
            // dd($form->getData());
            // $user = $form->getData();

            $plainPassword = ($form['plainPassword']->getData());
            // dd($plainPassword);
            // dd($form);
            // dd($encoder);

            $hash = $encoder->hashPassword($user, $plainPassword);  
            // dd($hash);

            // Pourquoi $user? Parce qu'elle représente l'entité et, de plus, nommée dans le encoders de security.yaml
            // En second paramètre, je demande de récupérer le password afin donc de le hacher
            // Étapes : 1) Je déclare la variable $hash
                    //  2) Je récupère $encoder qui appellera la méthode hashPassword()
                    //  3) En paramètre, appeler $user qui représente l'entité User, et enfin, de par cette variable, récupérer le plainPassword
                    //  4) Le résultat : $hash -> aura un password haché

            $user->setPassword($hash); // On demande que $user modifie ou ajoute le mot de passe soumis, haché


            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('security_login');
        }

        return $this->renderForm('security/register.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * @Route("/login", name="security_login")
     */
     public function login() {
         
        return $this->render('security/login.html.twig');
    }

}
