<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo): Response
    {
        // $repo = $this->getDoctrine()->getRepository(Article::class);

        // Exemple de recherche
        // $article = $repo->find(12); // Recherche de l'article 12

        // $article = $repo->findOneByTitle('Titre de l\'article'); // Recherche un titre dont le titre correspondrait à celui-ci

        // $articles = $repo->findOneByTitle('Titre de l\'article'); // Recherche ayant ce titre donc la variable est au pluriel
        

        $articles = $repo->findAll(); // Recherche tous les articles
        
        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/home2", name="home2")
     */
    public function home() {
        // $this->render('') appelle un fichier twig
        return $this->render('blog/home.html.twig', [
            'title' => 'Bienvenue ici les amis!!',
            'age' => 31

        ]);
    }
    
    /**
     * @Route("/blog/new", name="blog_create")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */
    public function form(Article $article = null, Request $request, EntityManagerInterface $manager){

        if(!$article){
            $article = new Article();
        }

        // $form = $this->createFormBuilder($article) 
        //              ->add('title', TextType::class, [ // On peut ajouter aussi des options d'affichages avec attr(attribut HTML), un placeholder
        //                 'attr' => [
        //                     'placeholder' => 'Titre de l\'article'
        //                 ]
        //              ])
        //             //  ->add('content') // autre exemple de content ci-dessous
        //             ->add('content', TextareaType::class, [  // TextType::class est une autre possibilité que le textarea voir doc Symfony (différents types comme EmailType,...)
        //                 'attr' => [
        //                         'placeholder' => 'Contenu de l\'article'
        //                 ]
        //              ])
        //              ->add('image', TextType::class, [
        //                 'attr' => [
        //                         'placeholder' => 'Image de l\'article'
        //                 ]
        //              ])
        //              ->getForm(); // Envoie moi ce que je viens de te demander (getForm())

        // Tips : commentez les lignes du dessus 
        // Utiliser un createForm et non un createFormBuilder comme ci-dessus -> function form
        $formArticle = $this->createForm(ArticleType::class, $article);

        $formArticle->handleRequest($request); // Analyse si tout va bien, champs bien rempli, s'il a été envoyé, etc

        if($formArticle->isSubmitted() && $formArticle->isValid()) { // Si le formulaire est soumis ou s'il est valide alors...

            if (!$article->getId()) { // Si l'article n'a pas d'identifiant alors créé le avec une date 
                $article->setCreatedAt(new \Datetime());
            }
           
            $manager->persist($article);

            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }

        return $this->renderForm('blog/create.html.twig', [  // ATTENTION !!!! renderForm remplace render(), incluant, aussi, l'ancien paramètre createView()
                                                            // // -> formArticle->createView(), Symfony 5.3

            // On passe dans un tableau le "formArticle" (que l'on ajoutera dans le formulaire méthode post de la page create) 
            // qui va créer une vue pour le formulaire que l'on ajoute en plus de la vue $this->render('blog/create.html.twig'
            'formArticle' => $formArticle,
            'editMode' => $article->getId() !== null  // Utilisation d'un booléen, si l'id d'article est différent de null alors il est à true
                                                      // ce qui va servir pour changer le btn 'Ajouter un article' ou 'Modifier un article'
        ]);

    }
    
    // EXEMPLE POUR GAGNER DU TEMPS PAR RAPPORT À CI-DESSOUS, EN RÉCUPÉRANT LA CLASSE ARTICLE ET EN SUPPRIMANT LE $REPO!!!
    /**
     * @Route("/blog/{id}", name="blog_show")
     */
     public function show(Article $article, Request $request, EntityManagerInterface $manager){

        $comment = new Comment();
         
        $commentForm = $this->createForm(CommentType::class, $comment);

        $commentForm->handleRequest($request); // Analyse si tout va bien, champs bien rempli, s'il a été envoyé, etc

        if($commentForm->isSubmitted() && $commentForm->isValid()) { // Si le formulaire est soumis et s'il est valide alors...

            $comment->setCreatedAt(new \Datetime())  // Je demande que $comment qui représente ma classe crée une date
                    ->setArticle($article);          // Je l'envoie dans mon article
           
            $manager->persist($comment);

            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]); // Je redirige vers la page et en paramètre l'id de l'article
        }

        return $this->renderForm('blog/show.html.twig', [    // ATTENTION !!!! renderForm remplace render(), incluant, aussi, l'ancien paramètre createView()
                                                            // -> commentForm->createView(), Symfony 5.3        
            'article' => $article, 
            'commentForm' => $commentForm
        ]);
    }

    // /**
    //  * @Route("/blog/{id}", name="blog_show")
    //  */
    // public function show($id){
    //     $repo = $this->getDoctrine()->getRepository(Article::class);

    //     $article = $repo->find($id);

    //      return $this->render('blog/show.html.twig', [
    //         'article' => $article,
    //      ]);
    // }


}
