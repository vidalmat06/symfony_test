<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\EditUserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @Route("/user")
 * @IsGranted("ROLE_ADMIN")
 */
class UserController extends AbstractController
{
    /**
     * @Route("", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        $user = $userRepository->findall();
        // if(!$user)
        //         {
        //                 $session->set("message", "Merci de vous connecter");
        //                 return $this->redirectToRoute('home');
        //         }

        //         else if(in_array('ROLE_ADMIN', $user->getRoles())){
        //                 return $this->render('navigation/adminSpace.html.twig', [
        //                         'user' => $userRepository->findAll(),
        //                 ]);
        //         }

        //         else if(in_array('ROLE_USER', $user->getRoles())){
        //                 return $this->render('navigation/userSpace.html.twig', [
        //                         'user' => $userRepository->findAll(),
        //                 ]);
        //         }
        return $this->render('user/index.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $encoder): Response
    {
        // $user = new User();
        // $form = $this->createForm(UserType::class, $user);
        // $form->handleRequest($request);

        // if ($form->isSubmitted() && $form->isValid()) {
        //     $entityManager = $this->getDoctrine()->getManager();
        //     $entityManager->persist($user);
        //     dd($form);
        //     $entityManager->flush();

        //     return $this->redirectToRoute('user_index');
        // }

        // return $this->renderForm('user/new.html.twig', [
        //     'user' => $user,
        //     'form' => $form
        // ]);

        $user = new User();

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(['ROLE_USER']);
            // dd($form->getData());
            // $user = $form->getData();

            $plainPassword = $form['plainPassword']->getData();
            // dd($plainPassword);
            // dd($form);
            // dd($encoder);

            $hash = $encoder->hashPassword($user, $plainPassword);
            // dd($hash);

            // Pourquoi $user? Parce qu'elle représente l'entité et, de plus, nommée dans le encoders de security.yaml
            // En second paramètre, je demande de récupérer le password afin donc de le hacher
            // Étapes : 1) Je déclare la variable $hash
            //  2) Je récupère $encoder qui appellera la méthode encodePassword()
            //  3) En paramètre, appeler $user qui représente l'entité User, et enfin, de par cette variable, récupérer le getPassword
            //  4) Le résultat : $hash -> aura un password haché

            $user->setPassword($hash); // On demande que $user modifie ou ajoute le mot de passe soumis, haché

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->renderForm('user/new.html.twig', [
            'form' => $form,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(EditUserType::class, $user);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            
            $this->getDoctrine()->getManager()->flush();
            $flash = $this->addFlash('success', 'Le profil a bien été modifié');

            return $this->redirectToRoute('user_index');
        }

        return $this->renderForm('user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);

        // $plainPassword = ($form['plainPassword']->getData());
        //     // dd($plainPassword);
        //     // dd($form);
        //     // dd($encoder);

        //     $hash = $encoder->hashPassword($user, $plainPassword);  
        //     // dd($hash);

        //     // Pourquoi $user? Parce qu'elle représente l'entité et, de plus, nommée dans le encoders de security.yaml
        //     // En second paramètre, je demande de récupérer le password afin donc de le hacher
        //     // Étapes : 1) Je déclare la variable $hash
        //             //  2) Je récupère $encoder qui appellera la méthode encodePassword()
        //             //  3) En paramètre, appeler $user qui représente l'entité User, et enfin, de par cette variable, récupérer le getPassword
        //             //  4) Le résultat : $hash -> aura un password haché

        //     $user->setPassword($hash); // On demande que $user modifie ou ajoute le mot de passe soumis, haché

        //     $manager->persist($user);
        //     $manager->flush();

        //     return $this->redirectToRoute('user_index');
        // }

        // return $this->renderForm('user/edit.html.twig', [
        //     'form' => $form
        // ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"POST"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        $flash = $this->addFlash('warning', 'Le profil a bien été supprimé');

        return $this->redirectToRoute('user_index');
    }
}
