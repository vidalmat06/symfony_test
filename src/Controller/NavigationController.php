<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class NavigationController extends AbstractController
{
        /**
         * @Route("/", name="home")
         */
        public function index(UserRepository $userRepository, Session $session): Response
        {
                //besoin de droits admin
                $user = $this->getUser();
                // dd($user);
                if(!$user)
                {
                        $session->set("message", "Merci de vous connecter");
                        return $this->redirectToRoute('home2');
                }

                else if(in_array('ROLE_ADMIN', $user->getRoles())){
                        return $this->render('admin/adminSpace.html.twig', [
                                'user' => $userRepository->findAll(),
                        ]);
                }

                else if(in_array('ROLE_USER', $user->getRoles())){
                        return $this->render('navigation/userSpace.html.twig', [
                                'user' => $userRepository->findAll(),
                        ]);
                }


                return $this->redirectToRoute('index');

                // return $this->render('navigation/index.html.twig');
        }

        /**
         * @Route("/adminSpace", name="adminSpace")
         * @IsGranted("ROLE_ADMIN")
         */
        public function admin()
        {
                //test si un utilisateur est connecté
                $this->denyAccessUnlessGranted('ROLE_ADMIN');
                return $this->render('admin/adminSpace.html.twig');
        }


        /**
         * @Route("/userSpace", name="userSpace")
         * @IsGranted("ROLE_USER")
         */
        public function membre()
        {
                //test si un utilisateur est connecté
                $this->denyAccessUnlessGranted('ROLE_USER');
                return $this->render('navigation/userSpace.html.twig');
        }
}