<?php

namespace App\DataFixtures;

// use Faker\Factory;
use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create('fr_FR'); // Passe des données en français('FR_fr')

        // Créer 3 catégories fakers
        for($i = 1; $i <= 3; $i++) {
            $category = new Category();
            $category->setTitle($faker->sentence())  // Se référer à la doc pour sentence()
                     ->setDescription($faker->paragraph());

            $manager->persist($category);
        

            // Créer entre 4 et 6 articles
            for($j = 1; $j <= mt_rand(4, 6); $j++) {  // mt_rand() signifie entre 4 et 6 catégories

                $article = new Article();

                // '.=' signifie ajouter en plus de ce que contient déjà la variable
                // On créé une variable content qui contiendra cinq paragraphes faker et on l'appelle dans le setter
                // $content = '<p>' . join($faker->paragraph(5), '<p></p>') . '</p>'; 

                $article->setTitle($faker->sentence())
                        ->setContent($faker->paragraphs($nb = 3, $asText = true))
                        ->setImage($faker->imageUrl())
                        ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                        ->setCategory($category);
                        
                $manager->persist($article);
            
                
            // Créer des commentaires à l'article
                for($k = 1; $k <= mt_rand(4, 10); $k++) {  // mt_rand() signifie entre 4 et 10 commentaires

                    $comment = new Comment();

                    // $now = new \Datetime();
                    // $interval = $now->diff($article->getCreatedAt()); // diff() représente l'intervalle entre la date de la création de l'article et maintenant 
                    // $days = $interval->days; // Ajoute l'interval et le nombre de jours
                    // $minimum = '-' . $days . 'days';  // ce qui va représenter -100 jours 

                    $comment->setAuthor($faker->name())
                            ->setContent($faker->paragraphs($nb = 3, $asText = true))
                            ->setCreatedAt($faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now', $timezone = null))
                            ->setArticle($article);
                            
                    $manager->persist($comment);
                }
            }
        }
        $manager->flush();
    }

    // ANCIEN MODELE FIXTURE POUR SEULEMENT L'ARTICLE
    // public function load(ObjectManager $manager): void
    // {
    //     for($i = 1; $i <= 10; $i++) {
    //         $article = new Article();
    //         $article->setTitle("Titre de l'article n°$i")
    //                 ->setContent("<p>Contenu de l'article n°$i</p>")
    //                 ->setImage("https://via.placeholder.com/")
    //                 ->setCreateAt(new \Datetime());  // Ne pas oublier l'antislashes
    //         $manager->persist($article);
    //     }

    //     $manager->flush();
    // }
}
