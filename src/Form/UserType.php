<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserType extends AbstractType
{
    /**
    * @var AuthorizationChecker
    */
    private $authorizationChecker=null;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
      $this->authorizationChecker = $authorizationChecker;
    }


    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if(!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $builder
                ->add('username', null, [
                        'label' => 'Nom'
                ])
                ->add('email')
                ->add('plainPassword', RepeatedType::class, array(
                'type'              => PasswordType::class,
                'mapped'            => false,
                'first_options'     => array('label' => 'Mot de passe'),
                'second_options'    => array('label' => 'Confirmation du mot de passe'),
                'invalid_message' => 'Les champs du mot de passe doivent correspondre',
                "allow_extra_fields" => true,
                ))
            ;
        }else{
            $builder
                ->add('username', null, [
                        'label' => 'Nom'
                ])
                ->add('email')
                ->add(
                    'roles', ChoiceType::class, [
                        'choices' => ['ADMIN' => 'ROLE_ADMIN', 'USER' => 'ROLE_USER'],
                        'label' => 'Rôle',
                        'expanded' => true,
                        // 'multiple' => true,
                    ]
                )
                ->add('plainPassword', RepeatedType::class, array(
                'type'              => PasswordType::class,
                'mapped'            => false,
                'first_options'     => array('label' => 'Mot de passe'),
                'second_options'    => array('label' => 'Confirmation du mot de passe'),
                'invalid_message' => 'Les champs du mot de passe doivent correspondre',
                "allow_extra_fields" => true,
                ))
            ;

            // Data transformer
            $builder->get('roles')
                ->addModelTransformer(new CallbackTransformer(
                    function ($rolesArray) {
                        // Transforme le tableau en chaîne
                        return count($rolesArray)? $rolesArray[0]: null;
                    },
                    function ($rolesString) {
                        // Transforme la chaîne en un tableau
                        return [$rolesString];
                    }
                ))
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            // enable/disable CSRF protection for this form
            'csrf_protection' => true,
            // the name of the hidden HTML field that stores the token
            'csrf_field_name' => '_token',
            // an arbitrary string used to generate the value of the token
            // using a different string for each form improves its security
            'csrf_token_id'   => 'task_item',
        ]);
    }
}
