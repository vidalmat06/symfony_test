###### INSTALLATION DU PROJET
Voici les étapes pour installer le projet sur votre poste :

* Créer un nouveau projet avec la commande :
    - symfony new --full NomDuProjet

* Aller à la racine 

* Installer le composant des Fixtures :
    composer require orm-fixtures --dev

* Installer "le Bro", commande pour la librairie server
    - composer require symfony/web-server-bundle --dev ^4.4.37
    <!-- Vous pouvez vérifier la dernière version ici : https://packagist.org/packages/symfony/web-server-bundle -->

* Ajouter la "Debugbar" de SYMFONY :
    composer require profiler --dev

* Composer cette ligne qui générera le validator :
    composer require symfony/validator

* Pour mettre à jour les packages :
    composer update

* Ajouter la protection CSRF : 
    composer require twig-bundle security-csrf

* Aller dans config/packages/framework.yaml :
    framework:
    # ...
        csrf_protection: true

* Ne surtout pas oublier d'ajouter ceci à tout form de type "MonFormType" dans la fonction configureOptions :
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'      => Task::class,
            // enable/disable CSRF protection for this form
            'csrf_protection' => true,
            // the name of the hidden HTML field that stores the token
            'csrf_field_name' => '_token',
            // an arbitrary string used to generate the value of the token
            // using a different string for each form improves its security
            'csrf_token_id'   => 'task_item',
        ]);
    }

* Installer la librairie pour réinitialiser le mot de passe : 
    composer require symfonycasts/reset-password-bundle

* Ensuite les fichiers correspondants au ResetPasswordRequestFormType et le ChangePasswordFormType ;
    php bin/console make:reset-password

* Décommenter dans le .env, la partie Database MySQL et ne pas oublier d'enlever le mdp et de changer le nom de la BDD 

###### FIN DE L'INSTALLATION DU PROJET


<!------------------------ BDD ------------------------>

* Créer sa BDD : 
    php bin/console doctrine:database:create (cette commande reprendra le nom de la Bdd du .env)

* Création des tables :
    php bin/console make:entity : 
        - nom de la table (Article)

        * Les attributs seront demandés :
            - nom de la propriété (title)
            - type (string) => '?' sert à connaître tous les types
            - longueur (255)
            - champ null ou pas (no)
            - Ensuite proposition d'un autre attribut etc...

        * Nouvel attribut ("content", text, no)

        * Nouvel attribut ("image", string, 255, no)

        * Nouvel attribut ("createdAt", datetime ou datetime_immutable(identique), no)

<!-- Le repository ainsi que l'Entity ont été créés -->

* Faire la migration :
    php bin/console make:migration

* Envoyer une fois vérifié vers la BDD MySQL grâce à Doctrine : 
    php bin/console doctrine:migrations:migrate

* Créer une fixture pour article : 
    php bin/console make:fixtures

* Se reporter dans le dossier Datafixtures et ouvrir le fichier ArticleFixtures, l'ajout du code pour les données fictives en 'set'

* Charger le ou les fixtures :
    php bin/console doctrine:fixtures:load
    <!-- Ne pas oublier qu'en faisant cette commande, la BDD sera purgée d'où la question posée avant de confirmer  -->

<!------------------------ FIN BDD ------------------------>


<!------------------------ AFFICHAGES / CONTROLLERS ------------------------>

* Récupérer les articles, pour cela, aller dans le controller BlogController et ajouter cette ligne dans la function index (plusieurs exemples pour la récupération ligne 27)
        $repo = $this->getDoctrine()->getRepository(Article::class);
        <!-- Ne pas oublier d'importer la classe Article -->

* Ajouter ensuite une variable $article contenant les articles (ligne 41) dans le return 

* Supprimer les articles et n'en garder qu'un et l'envelopper dans une boucle for dans la page index.html.twig du dossier Blog

* Changer le titre par cette ligne <h2>{{ article.title }}</h2>, elle récupérera tous les articles avec leurs numéros 

* Changer les <p> par cette ligne {{ article.content | raw }}, elle récupérera tous les contents 
<!-- filtre raw servant à afficher tel quel sans les balises <p> -->

* Idem pour les images <img src="{{ article.image }}" alt="">

* De même pour la date, <div class="metadata">Écrit le {{ article.createdAt | date('d/m/y') }} à 12h00 dans la catégorie Politique</div>,  
<!-- Attention, ne pas oublier de mettre le filtre twig "| date('d/m/y')" pour le Datetime sinon rien ne s'affichera!!! -->

* Même chose pour l'heure, {{ article.createdAt | date('H:i')}}

* Pour la partie show, afin d'enlever les chiffres dans l'URL, on va passer sur le BlogController(voir le controller function show) :
    <!-- On modifie la route en lui passant un id en paramètre qui correspondra au numéro de l'article  -->
    /**
     * @Route("/blog/{id}", name="blog_show")
     */

    Ne pas oublier de passer en paramètre la fonction avec un $id
    Ajouter une variable dans la fonction pour récupérer le repo pour aller chercher dans la classe Article 
    Et enfin une variable $article qui récupérera tous les repos avec avec la variable $id
    
    $repo = $this->getDoctrine()->getRepository(Article::class);

    $article = $repo->find($id);

    Ne pas oublier d'ajouter un tableau dans le renderform pour dire qu'un 'article => $article

* !!! ATTENTION !!! Ne pas oublier que le path utilisé dans l'index.html.twig a besoin de deux paramètres, sinon on aura une erreur si on affiche que blog dans l'URL sans un numéro 
    Ajouter <a href="{{ path('blog_show', {'id': article.id})}}" class="btn btn-primary">Lire la suite</a>
    <!-- On dit tout simplement, en plus de la route, dans le paramètre 'id', tu dois passer article.id -->

<!------------------------ FIN AFFICHAGES / CONTROLLERS ------------------------>



<!------------------------ FORMULAIRE  ------------------------>


* Création dans BlogController d'une fonction form, bien penser à la placer au-dessus de la fonction show, pour la simple et bonne raison que le nom de la route étant new, il pourrait se baser sur l'id de la fonction show et donc lors de son appel, il afficherait show au lieu de create de par le nom de sa route!!!

* Il est possible d'ajouter le thème de twig pour symfony via la doc, ensuite aller dans config/packages/twig.yaml et ajouter à la suite de twig, cette ligne :
    form_themes: ['bootstrap_4_layout.html.twig']
    <!-- Ne pas oublier de rajouter le thème sur la page utilisé en dessous du extends => {% form_theme form 'bootstrap_4_layout.html.twig' %} -->

* Petit bonus : Grâce à la CLI, on peut créer un formulaire directement : 
    php bin/console make:form => Nom du formulaire => Et s'il dépend d'une classe
    <!-- À noter que le fichier se trouvera, par la suite, dans le dossier Form dans l'éditeur, le résultat est que l'on obtienne le même formulaire -->
    <!-- fait dans le BlogController function form -->

<!------- VALIDATION FORMULAIRE  ------->

* Pour la partie validation avec les @Assert, pour cela, il faut aller dans la classe Article et ajouter un namespace (use), doc => https://symfony.com/doc/current/validation.html#sln : 
    use Symfony\Component\Validator\Constraints as Assert;

* Différents @Assert sont disponibles(voir la doc), dans notre cas : 
    Pour le titre   => * @Assert\Length(min=10, max=255)
    Pour le contenu => * @Assert\Length(min=10)
    Pour l'image    => * @Assert\Url()


<!------------------------ FIN FORMULAIRE  ------------------------>


<!------------------------ ENTITÉS ET RELATIONS  ------------------------>

* Création d'une entité "Category" avec la CLI : 
    php bin/console make:entity -> Category
        title->string->255->nullable(no)
        description->text->nullable(yes)
        <!-- Ici on va demander une relation entre la classe Category et Article, pour mieux choisir, on peut taper "?"  -->
        articles->relation->Article(classe)->OneToMany(chaque catégorie peut avoir plusieurs articles)->category(nom qui aura le lien avec les catégories)(yes)->nullable(no)
        ->orphanRemoval(no)

* Attention, ne pas oublier, lors d'un ajout d'une table sur une BDD existante, si l'on se base sur la seconde migration, les requêtes SQL, définiront un id en NOT NULL, hors ce qu'il ne faut pas oublier, c'est que si la BDD a déjà été alimentée, ça causera un gros soucis, donc dans ce cas, il faudra modifier les ID :
    $this->addSql('ALTER TABLE article ADD category_id INT NULL');
    <!-- On enlève le NOT NULL par NULL, cela dira simplement que l'id pourra être nul  -->

* Création d'une entité "Comment" avec la CLI : 
    php bin/console make:entity -> Comment
        author->string->255->nullable(no)
        content-text->nullable(no)
        createdAt-datetime->nullable(no)
        <!-- Ici on va demander une relation entre la classe Comment et Article, pour mieux choisir, on peut taper "?"  -->
        article->relation->Article(classe)->ManyToOne(chaque commentaire doit s'adresser à un seul article)->nullable(no)->getcomment(nom qui aura le lien avec les catégories)(yes)
        ->comments(yes)(nom dans la classe Article)->orphanRemoval(yes)


<!------- LIBRAIRIE FAKER  ------->

* Pour utiliser la librairie Faker pour les données fictives avancées :
    composer require fzaninotto/faker
    <!-- Se référer à la doc GitHub pour les différents fakers(title, name, content, etc...) -->

* Ensuite dans le dossier DataFixture/ArticleFixtures : 
    Ajouter en première ligne dans la function load : 
        $faker = Faker\Factory::create('FR_fr'); // Passe des données en français('FR_fr')

* Ajouter un for pour la fixture ArticleFixture, avec Category, Comment, et modifier Article en l'intégrant également avec le reste dans le load : 
    Voir function load() 
    <!-- Attention, il y aura 3 "for" dans cette fixture, bien penser à indenter correctement les boucles, tout comme toutes les fermer qu'à la fin car une boucle for contiendra les deux autres!!! -->


<!------- EDIT CATEGORY  ------->

* Pour la modification d'un article via la page https://localhost:8000/blog/1/edit, il faut ajouter un champ dans Form/ArticleType.php :
    <!-- On ne peut pas juste appeler 'category' sinon Symfony ne va pas aimer, pour cela, il faut ajouter des champs voir via doc Symfony/forms (https://symfony.com/doc/current/reference/forms/types/entity.html) -->
    Ajouter la classe en plus de 'category' => EntityType (type d'entité donc Category) qui sera sous forme d'array [](tableau d'options) (ligne 19):
        Seront ajouté -> 'class' qui sera donc Category
                      -> 'choice_label' qui sera 'title'

<!------- FIN CATEGORY  ------->


<!------------------------ FIN ENTITÉS ET RELATIONS  ------------------------>



<!------------------------ AUTHENTIFICATIONS  ------------------------>

<!------- SÉCURITÉ  ------->

* Création d'une nouvelle entité User : 
    php bin/console make:entity OU php bin/console make:entity User
        email->string->255->nullable(no)
        username->string->255->nullable(no)
        password->string->255->nullable(no)

* Effectuer ensuite la migration comme exemples précédents :
    php bin/console make:migration

* Le fichier étant créé, on demande d'appliquer la migration dans la BDD :
    php bin/console doctrine:migrations:migrate

* Fabrication d'un formulaire d'inscription : 
    php bin/console make:form RegisterType
    -> Nom de l'entité auquel il sera affilié => User

* Aller dans le fichier se trouvant dans Form/RegisterType.php :
    Ajouter en plus un champ ->add('confirm_password')

* Ajouter dans Entity/User.php, une propriété confirm_password (sans @ORM, pour bien signifier qu'elle n'aura aucun rapport avec la BDD) :
    private $confirm_password 

    public function getConfirmPassword(): ?string
    {
        return $this->condirm_password;
    }

    public function setConfirmPassword(string $condirm_password): self
    {
        $this->condirm_password = $condirm_password;

        return $this;
    }

    <!-- Ne pas oublier les getters et les setters, sinon mettre la variables en public, dans ce cas il n'est pas obligé de les mettre -->

* Créer un nouveau controller qui gérera la sécurité : 
    php bin/console make:controller->SecurityController

* Supprimer la funcion index() et en créé une nouvelle du nom de register() : 
    - Dans cette fonction on ajoutera une variable $form pour créer un formulaire sur la base de formulaire créé précédemment, Register::class
    <!-- Bien entendu pour faire comprendre que ce formulaire créera un nouvel utilisateur voir ci-dessous -->
    - Nommer une variable $user = new User;
    - Ajouter bien sûr le $user en deuxième paramètre, dans le createForm fait ci-dessus
    <!-- Ce qui relie les champs du formulaire, au champ de l'utilisateur -->
    - Ne surtout pas oublier d'ajouter un return de la vue security/register.html.twig, penser à ajouter sous forme de tableau
    'form' qui représentera la variable $form et qui appellera la méthode createForm (pour créer la vue)

* Ajouter dans le dossier templates/security, un nouveau fichier, celui qui nous manque par rapport au retour de la vue ci-dessus
    register.html.twig

* Ajouter le formulaire dans la page register, on peut aussi y ajouter les placeholders('attr'->attribut) et les labels (Ne pas oublier de mettre le form_start et le form_end)

* Le mieux aussi est de retourner dans RegisterType pour modifier le champ des passwords afin de lui ajouter l'option PasswordType::class pour bien avoir un champ de type password, autrement dit que lors de l'écriture, les caractères soient cachés, une fois que l'on saisie 

* Ne pas oublier que si l'on veut submit, il faut ajouter en paramètre de la fonction register() de SecurityController : 
    public function register(Request $request, EntityManagerInterface $manager)

* Ensuite demander que mon formulaire $form analyse la Request que je passe ici dans $request : 
    $form->handleRequest($request);

* J'utilise un if qui va dire -> 'Si mon formulaire est soumis ET s'il est valide', alors, je veux que tu fasses persister $user inclus dans $manager : 
    if($form->isSubmitted() && $form->isValid()) {
        $manager->persist($user);
        $manager->flush(); // Enregistre et envoie
    }

<!------- FIN SÉCURITÉ  ------->

<!------- VALIDATION  ------->

* Maintenant il faut ajouter des validations, dans User.php, ajouter :
    use Symfony\Component\Validator\Constraints as Assert;
    <!-- Les annotations Constraints permettent de valider les champs d'un formulaire -->

* J'ajoute des contraintes, notamment pour le password, en dessous des routes (https://symfony.com/doc/current/reference/constraints.html) : 
    @Assert\Length(min=8, minMessage="Votre mot de passe doit comporter au minimum 8 caractères")
    <!-- Length valide la longueur d'une chaîne donnée, on peut avoir également minMessage ou maxMessage -->

* De même, une nouvelle contrainte dans confirm_password, pour vérifier que les deux champs soient identiques : 
    @Assert\EqualTo(propertyPath="password, message="Votre mot de passe doit être identique"")
    <!-- Cet Assert vérifie que ce champ doit être égal à une autre valeur, dans notre cas "password"
    (https://symfony.com/doc/current/reference/constraints/EqualTo.html)  -->
 
<!------- FIN VALIDATION  ------->

<!------- HACHAGE PASSWORD  ------->

* Ici, très important, les passwords envoyées dans la BDD ne sont pas encore cryptées, en cas de piratage, ils seraient en clairs :
    Aller dans config/packages/security.yaml -> ajouter dessous security, un encoder qui passera par l'entité User et utilisera le hachage bcrypt :
        password_hashers: 
            App\Entity\User:
                algorithm: 'auto'
        <!-- !!!! Attention, extrêmement important, NE JAMAIS OUBLIER DANS CE FICHIER DE RESPECTER L'INDENTATION !!!! -->

* Dans le SecurityController, il faut modifier la méthode register :
    - Ajouter dans les paramètres de la fonction (UserPasswordEncoderInterface $encoder)
    - Avant de sauvegarder un utilisateur, il faut ajouter dans le if() de la méthode : 
        $hash = $encoder->encodePassword($user); (voir explication dans la méthode "en commentaire") 
    - On demande que $user modifie ou ajoute le mot de passe soumis, haché
        $user->setPassword($hash);

* À noter !!! IMPORTANT !!!, ne pas oublier d'implémenter la classe UserInterface dans la classe User ainsi que ses méthodes :
    class User implements UserInterface
                ...
    ///////////// interface methods ////////////////////
    /**
     * @see userInterface
     */
     public function getRoles() {
        return $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @see userInterface
     */
    public function getSalt() {
        //not needed if a modern algorithm is used 
    }
    /**
     * @see useInterface
     */
    public function getUsername() {
        //userName it's just a ID for debug bar
        return $this->email;
    }
    public function eraseCredentials() {
        //
    }

    <!-- Sinon la classe sera demandée pour le hachage et une erreur sera donnée -->

<!------- FIN HACHAGE PASSWORD  ------->


<!------- EMAIL UNIQUE  ------->

* Ajouter une contrainte pour email directement dans la l'entité User
    @Assert\Email()

* Et enfin ajouter une contrainte toujours dans l'entité User mais au-dessus de class User ligne 12 :
    @UniqueEntity(
        fields= {"email"},
        message= "L'email que vous avez indiqué est déjà utilisé"
    )
    <!-- Ne pas oublier d'importer la classe UniqueEntity dans le namespace -->

<!------- FIN EMAIL UNIQUE  ------->

<!------- DÉBUT LOGIN  ------->

* Aller dans config/packages/security.yaml :
    providers :
    ...
        in_database:
            entity:
                class: App\Entity\User
                property: email
    <!-- On nomme un providers qui peut s'appeler in_database, on lui dit de récupérer une entité, 
    on la nomme et ensuite la propriété pour aller chercher l'email -->

* Aller plus bas et rajouter dans main le providers qui sera récupéré :
    main:
        providers: in_database

* À la suite, ajouter form_login pour savoir où chercher le login mais aussi pour checker si tout va bien :

    form_login:
        login_path: security_login
        check_path: security_login
        <!-- Il s'agit bien de la même route car les deux se feront au même endroit -->

* Créer une page login dans le dossier security et ajouter une fonction login() dans SecurityController :
    /**
     * @Route("/Connexion", name="security_login")
     */
    public function login() {
        return $this->render('security/login.html.twig');
    }

* Créer le formulaire dans la page login en dur : 
    <input type="password"class="form-control w-50" id="password" name="_password" placeholder="Mot de passe" required>
    <!-- ATTENTION, toujours mettre _username pour email et _password dans les names car c'est ce que Symfony attend -->

<!------- FIN LOGIN  ------->

<!------- LOGOUT  ------->

* Ajouter ceci dans Config/packages/security.yaml :
    logout: 
        path: app_logout
        target: /
        invalidate_session: true // Passer de false à true
        delete_cookies:
            a: { path: null, domain: null }
            b: { path: null, domain: null }
            <!-- Bien respecter l'indentation!!! -->

* Et enfin ceci dans config/route.yaml :
    app_logout:
        path: /logout
        methods: GET

<!------- FIN LOGOUT  ------->

<!------- COMMENTAIRES  ------->

* Création d'un nouveau formulaire pour les commentaires :
    php bin/console make:form->CommentType->Comment

* Dans Form/CommentType, supprimer les champs 'createdAt' et 'article'

* Dans le BlogController et la fonction show(), ajouter ceci : 
    $comment = new Comment();
    $form = $this->createForm(CommentType::class, $comment);

* Ajouter dans cette même fonction, dans le renderForm, une autre vue en paramètre : 
    'commentForm' => $commentForm()
    <!-- ATTENTION !!!! renderForm remplace render() pour le formulaire, 
    incluant aussi le paramètre createView() -> commentForm->createView(), Symfony 5.3 -->

* Maintenant il faut que mon formulaire puisse être enregistré, envoyé sur la BDD et enfin affiché sur la page : 
    $commentForm->handleRequest($request); // Analyse si tout va bien, champs bien rempli, s'il a été envoyé, etc

        if($commentForm->isSubmitted() && $commentForm->isValid()) { // Si le formulaire est soumis ou s'il est valide alors...

            $comment->setCreatedAt(new \Datetime())  // Je demande que $comment qui représente ma classe crée une date
                    ->setArticle($article);          // Je l'envoie dans mon article
           
            $manager->persist($comment);

            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]); // Je redirige vers la page et en paramètre l'id de l'article
        }
        <!-- Ajouter ceci à la suite dans la fonction show -->

* Enfin on ajoute un {% if app.user %} pour que le formulaire ne soit visible que si l'utilisateur est connecté


<!------- FIN COMMENTAIRES  ------->


<!------- RESET PASSWORD  ------->

* Installer la librairie Symfonycasts pour réinitialiser le mot de passe : 
    composer require symfonycasts/reset-password-bundle

* Ensuite les fichiers correspondants au ResetPasswordRequestFormType et le ChangePasswordFormType ;
    php bin/console make:reset-password

* Ajouter dans config/packages/reset_password.yaml : *
    symfonycasts_reset_password:
    request_password_repository: App\Repository\ResetPasswordRequestRepository
    lifetime: 600
    throttle_limit: 600
    enable_garbage_collection: true
    <!-- La durée de validité du reset-password est définit par défaut à 1h soit 60mn, on peut le baisser en modifiant lifetime et throttle_limit, qui eux, sont en secondes donc 600 pour 10 mn-->

* TRES IMPORTANT!!! Il faut paramétrer Google via son compte pour rendre certains sites moins sécurisés utilisables, dans notre cas, le nôtre
Pour cela, il faut créer un mot de passe unique pour cette application afin que celle-ci soit accessible via Google Chrome et notre appli :
    https://myaccount.google.com/apppasswords?rapt=AEjHL4NWHeKg1RNrRCqc1PRWGEUN38VlGv5k--DPkwhPtluHyfod3qEtVqe-QVmmdNE7akMc4yKol-9r9JFrPeedtEYSf6ExIg
<!-- Ce lien permettra de créer un mot de passe Google pour un site moins sécurisé pour les tests de notre application, selectionner "autre (Nom personnalisé)", une fois le mot de passe obtenu, le mettre dans le .env voir étape suivante  -->

* Penser à décommenter dans le .env cette ligne pour les mailers Symfony Google :
    MAILER_DSN=smtp://localhost et remplacer par :
    MAILER_DSN=gmail+smtp://vidalmat06:motdepasseapplication@default
    <!-- Modèle pour Google Gmail = gmail+smtp://USERNAME:PASSWORD@default         voir doc (https://symfony.com/doc/current/mailer.html)-->

* Pour Gmail, installer le package de google pour les mails :
    composer require symfony/google-mailer

* La durée de validité du reset-password est définit par défaut à 1h soit 60mn dans le php.ini, on peut le baisser en modifiant cette ligne :
    default_socket_timeout = 10

* Enfin ne pas oublier de faire une migration pour implémenter l'entité ResetPasswordRequest en table dans la BDD

* 

<!------- FIN RESET PASSWORD  ------->


<!------- ROLES  ------->

* 

<!------- FIN ROLES  ------->



<!------------------------ FIN AUTHENTIFICATIONS  ------------------------>
