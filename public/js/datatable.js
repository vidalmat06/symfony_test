   // $(document).ready( function () {
    $('#dataTable').DataTable({
        "responsive":true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        }
    });
    // } );


// $("#dataTable").DataTable({
//     // autoFill: true,
//     columnDefs: [
//         {
//             targets: [0] /* column index [0,1,2,3]*/
//         }
//     ],
//     dom: "lBfrtip", // search builder + length + buttons + text search
//     buttons: [
//         {
//             className: "fas bg-success text-white border-0",
//             extend: "excel",
//             text: "&#xf1c3;"
//         },
//         {
//             className: "fas bg-danger text-white border-0",
//             extend: "pdf",
//             text: "&#xf1c1;"
//         },
//         {
//             className: "fas bg-secondary text-white border-0",
//             extend: "print",
//             text: "&#xf02f;"
//         },
//         {
//             className: "flex fas bg-primary text-white border-0 dt-presets",
//             text:
//                 '&#xf02e;<span id="nbPresets" class="position-absolute text-primary lh-base">0</span>'
//         },
//         // hidden-xs-up
//         {
//             className: "fas bg-dark text-white border-0 dt-sb",
//             extend: "searchBuilder",
//             text: "&#xf002;"
//         }
//     ],
//     colReorder: true, // enable table columns reordering
//     language: {
//         url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
//         searchBuilder: {
//             add: "+",
//             condition: "Condition",
//             clearAll: "Réinitialiser",
//             deleteTitle: "Supprimer",
//             data: "Colonne",
//             leftTitle: "Left",
//             logicAnd: "ET",
//             logicOr: "OU",
//             rightTitle: "Right",
//             title: {
//                 0: "Ajouter un filtre",
//                 1: "%d filtre",
//                 _: "%d filtres"
//             },
//             value: "Option",
//             valueJoiner: "et",
//             conditions: {
//                 num: {
//                     "=": {
//                         conditionName: "est égal à"
//                     }
//                 }
//             }
//         }
//     },
//     responsive: true, // enable responsive features
//     scrollY: 590, // 118 (row height) * 5 (rows)
//     stateSave: true, // saves the DataTable state
//     drawCallback: function() {
//         console.log('drawCallback');
//     }
// });